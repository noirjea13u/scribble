package vue;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import controleur.FabricantFigures;
import modele.DessinModele;
import modele.FigureColoree;
import modele.Quadrilatere;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe qui d�finit la vue (MVC)
 *
 */
public class VueDessin extends JPanel implements Observer{


	private DessinModele dessin; //dessin qui sera pr�sent sur la vue
	
	/**
	 * Constructeur de la vue
	 */
	public VueDessin() {
		super();
	}
	
	/**
	 * Getter de dessin
	 * @return le dessin modele
	 */
	public DessinModele getDessin() {
		return dessin;
	}


	@Override
	public void update(Observable o,Object arg1) {
		dessin = (DessinModele) o;
		repaint();
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(dessin != null) {
			
			for(FigureColoree f : dessin.getLfg()) {
				f.affiche(g);
			}
		}
	}
	
	public void construit(FigureColoree fc) {
		this.addMouseListener(new FabricantFigures(fc));
	}
	
	
	


}
