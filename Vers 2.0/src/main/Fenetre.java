package main;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controleur.PanneauChoix;
import modele.DessinModele;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe fenetre definissant l'interface utilisateur
 * et d�finissant aussi le main
 *
 */
public class Fenetre extends JFrame{

	private JPanel principal ; // panneau principal (o� se derouleront les dessins)
	private JPanel choix; // panneau des differents choix (choix de la couleur par ex)
	private VueDessin vdessin; // Vue MVC


	/**
	 * Constructeur de la classe
	 * @param s nom de la fen�tre
	 * @param w largeur de la fen�tre
	 * @param h hauteur de la fen�tre
	 */
	public Fenetre(String s, int w, int h) {
		super(s);
		vdessin = new VueDessin();
		choix = new PanneauChoix(vdessin);
		principal = new JPanel();
		
		principal.setLayout(new BorderLayout());
		principal.setPreferredSize(new Dimension(w,h));
		
		principal.add(vdessin, BorderLayout.CENTER);
		principal.add(choix, BorderLayout.NORTH);

		this.setContentPane(principal);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
	}

	
	/**
	 * Programme principal
	 * @param args tableau de chaines de caract�res contenant les
	 * param�tres pass�s � la m�thode main
	 */
	public static void main(String[] args) {
		Fenetre f = new Fenetre("Paint", 500,500);
		f.setVisible(true);
	}
	
}
