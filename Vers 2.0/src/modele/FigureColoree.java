package modele;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe abstraite qui est le sommet de la hierarchie des figures
 *
 */
public abstract class FigureColoree {

	private final static int TAILLE_CARRE_SELCTION = 4;//constante de la taille des carr�s de s�l�ction
	private boolean selected;//vrai si figure select, faux sinon
	protected Color couleur; // couleur de la figure
	protected Point[] tab_mem;// tabelau des points de m�morisation de la figure
	
	/**
	 * Constructeur vide
	 */
	public FigureColoree() {
		tab_mem = new Point[this.nbPoints()];
		couleur = Color.BLACK;
		selected = false;
	}
	
	/**
	 * M�thode asbtraite qui retourne le nb de points
	 * de m�morisation
	 * @return nb de points de m�morisation
	 */
	public abstract int nbPoints();
	
	/**
	 * M�thode asbtraite qui retourne le nb de clics n�cessaires
	 * � la construction d'une figure
	 * @return nb de clics de souris n�cessaires
	 */
	public abstract int nbClics();
	
	/**
	 * M�thode abstraite qui permet de modifier les pts
	 * de m�morisation � partir de pts de saisie
	 * @param ps tabl de pts qui remplacera le tableau tab_mem
	 */
	public abstract void modifierPoints(Point[] ps);
	
	
	/**
	 * m�thode qui affiche la figure s�l�ction�e
	 * � l'aide de petits carr�s
	 * @param g environnement graphique
	 */
	public void affiche (Graphics g) {
		if(selected) {
			g.setColor(Color.BLACK);
			for(int i =0; i<tab_mem.length; i ++) {
				g.fillRect(tab_mem[i].rendreX() - TAILLE_CARRE_SELCTION / 2, tab_mem[i].rendreY()- TAILLE_CARRE_SELCTION / 2,
						TAILLE_CARRE_SELCTION, TAILLE_CARRE_SELCTION);
			}
			g.setColor(couleur);
		}
	}
	
	/**
	 * M�thode qui s�lectionne la figure
	 */
	public void selectionne() {
		selected = true;
	}
	
	/**
	 * m�thode qui d�s�lectionne la figure
	 */
	public void deSelectionne() {
		selected = false;
	}
	
	/**
	 * m�thode qui change la couleur de la figure
	 * @param c nouvelle couleur
	 */
	public void changeCouleur(Color c) {
		couleur = c;
	}
	
	
	
}
