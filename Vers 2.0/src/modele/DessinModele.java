package modele;

import java.util.ArrayList;
import java.util.Observable;

import controleur.FabricantFigures;
import controleur.PanneauChoix;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe qui d�finit le mod�le de dessin
 * Toutes les instances de cette classe seront des dessins qui
 * comporteront des figures g�ometriques visualisables � l'�cran
 *
 */
public class DessinModele extends Observable {
	
	
	private ArrayList<FigureColoree> lfg;
	
	
	/**
	 * Constructeur du dessin mod�le
	 */
	public DessinModele() {
		super(); 
	}
	
	/**
	 * Getter de figure coloree
	 * @return une liste de figures
	 */
	public ArrayList<FigureColoree> getLfg(){
		return lfg;
	}
	
	/**
	 * Initialise la liste de figures colorees
	 */
	public void initDessinModele() {
		lfg = new ArrayList<FigureColoree>();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Permet d'ajouter une figure a la liste
	 * @param fc la figure a ajouter
	 */
	public void ajoute(FigureColoree f) {
		if(f != null) {
			
			for(FigureColoree fc : lfg) {
				fc.deSelectionne();
			}
			lfg.add(f);
			f.selectionne();
		}
		setChanged();
		notifyObservers();
	}
	
}
