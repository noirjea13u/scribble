package controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import modele.Cercle;
import modele.DessinModele;
import modele.FigureColoree;
import modele.Quadrilatere;
import modele.Triangle;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe d�finissant une partie du controleur
 * Classe construisant l'interface utilisateur
 *
 */
public class PanneauChoix extends JPanel{


	private DessinModele dmodele;// mod�le
	private VueDessin vdessin;//vue
	private FigureColoree fc;

	/**
	 * Constructeur de la classe
	 * @param vdessin de type DessinFigure
	 */
	public PanneauChoix(VueDessin vdessin) {
		dmodele = new DessinModele();
		this.vdessin = vdessin;
		dmodele.addObserver(vdessin);

		setLayout(new BorderLayout());

		final JRadioButton b1 = new JRadioButton("Nouvelle figure");
		final JRadioButton b2 = new JRadioButton("Trace a main levee");
		final JRadioButton b3 = new JRadioButton("Manipulations");

		ButtonGroup selection = new ButtonGroup();
		selection.add(b1);
		selection.add(b2);
		selection.add(b3);



		b1.setSelected(true);

		JComboBox choixFigure = new JComboBox(new String[] {"quadrilatere","triangle"
		});
		choixFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				b1.doClick();				
			}

		});

		JComboBox choixCouleur = new JComboBox(new String[] {
				"noir","rouge","bleu","vert","jaune"
		});



		choixCouleur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				fc.changeCouleur(determinerCouleur(choixCouleur.getSelectedIndex()));
				vdessin.repaint();
			}

		});

		dmodele.initDessinModele();

		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				switch(choixFigure.getSelectedIndex()) {
				case(0):
					fc = new Quadrilatere();
				break;


				case(1):
					fc = new Triangle();
				break;
				
				} 

				fc.changeCouleur(determinerCouleur(choixCouleur.getSelectedIndex()));

				vdessin.construit(fc);	

			}	
		});

		b2.setEnabled(false);
		b3.setEnabled(false);
		b1.doClick();

		JPanel haut = new JPanel();
		JPanel bas = new JPanel();

		haut.add(b1);
		haut.add(b2);
		haut.add(b3);
		bas.add(choixFigure);
		bas.add(choixCouleur);

		add(haut,BorderLayout.NORTH);
		add(bas,BorderLayout.SOUTH);

	}


	/**
	 * m�thode qui determine la couleur � utiliser
	 * @param index repr�sentant la couleur � utiliser
	 * @return la couleur choisie par l'utilisateur
	 */
	private Color determinerCouleur(int index) {
		switch(index) {
		case 0:
			return Color.black;
		case 1:
			return Color.RED;
		case 2:
			return Color.blue;
		case 3:
			return Color.green;
		case 4:
			return Color.yellow;
		default:
			return Color.BLACK;
		}
	}

}
