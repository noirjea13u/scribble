package controleur;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import modele.*;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe impl�mentant la cr�ation de 
 * figures g�om�triques via des clics de souris
 *
 */
public class FabricantFigures implements MouseListener {

	private int nb_points_cliques;//Accumule le nb de clics de souris
	private Point[] points_cliques;// tableau contenant des points cr�es � partir de clics de souris
	private FigureColoree figure_en_cours_de_fabrication; // figures en cours de fabrication

	/**
	 * Constructeur de la classe
	 * @param f figure vide cr�e dans la classe dialogue
	 */
	public FabricantFigures(FigureColoree f) {
		figure_en_cours_de_fabrication = f;
		nb_points_cliques = 0;
		points_cliques = new Point[f.nbClics()];
	}


	/**
	 * M�thodes de l'interface MouseListener non utilis�es.
	 * @param e
	 */
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}
	public void mouseClicked(MouseEvent e) {	
	}

	/**
	 * M�thode impl�mentant la cr�ation d'une figure g�om�trique via des clics de souris.
	 * @param e
	 */
	public void mousePressed(MouseEvent e) {
		if(figure_en_cours_de_fabrication != null) {
			if(nb_points_cliques<points_cliques.length-1) {
				points_cliques[nb_points_cliques] = new Point(e.getX(), e.getY());
				nb_points_cliques = nb_points_cliques +1;
			}else {
				points_cliques[nb_points_cliques] = new Point(e.getX(), e.getY());
				figure_en_cours_de_fabrication.modifierPoints(points_cliques);
				DessinModele dm = (((VueDessin)(e.getSource())).getDessin());
				dm.ajoute(figure_en_cours_de_fabrication);
				((VueDessin)(e.getSource())).removeMouseListener(this);
			}
		}


	}
}
