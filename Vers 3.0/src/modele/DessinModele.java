package modele;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Observable;

import controleur.FabricantFigures;
import controleur.PanneauChoix;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 3.0
 * Classe qui d�finit le mod�le de dessin
 * Toutes les instances de cette classe seront des dessins qui
 * comporteront des figures g�ometriques visualisables � l'�cran
 *
 */
public class DessinModele extends Observable {
	
	
	private ArrayList<FigureColoree> lfg;
	private int nbf; // Nombre effectif de figures apparaissant dans ce dessin
	private int sel; // Indice de la figure acuellement s�lectionn�e (-1 si aucune figure n'est s�lectionn�e)
	
	
	/**
	 * Constructeur du dessin mod�le
	 */
	public DessinModele() {
		super(); 
		nbf = 0;
		sel = -1;
	}
	
	/**
	 * Getter de figure coloree
	 * @return une liste de figures
	 */
	public ArrayList<FigureColoree> getLfg(){
		return lfg;
	}
	
	/**
	 * Initialise la liste de figures colorees
	 */
	public void initDessinModele() {
		lfg = new ArrayList<FigureColoree>();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Permet d'ajouter une figure a la liste
	 * @param fc la figure a ajouter
	 */
	public void ajoute(FigureColoree f) {
		if(f != null) {
			
			for(FigureColoree fc : lfg) {
				fc.deSelectionne();
			}
			lfg.add(f);
			f.selectionne();
			nbf += 1;
			sel += 1;
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Cette m�thode permet de changer 
	 * la couleur de la figure pass�e en param�tre.
	 * @param fc figure � laquelle il faut changer la couleur.
	 * @param coul nouvelle couleur.
	 */
	public void changeCoul(FigureColoree fc, Color coul) {
		fc.changeCouleur(coul);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @return le nombre de figure
	 */
	public int getNbf() {
		return nbf;
	}

	/**
	 * @return l index de la figure selectionnee
	 */
	public int getSel() {
		return sel;
	}
	
	/**
	 * definis le nombre de figures
	 * @param nbf nombre de figures
	 */
	public void setNbf(int nbf) {
		this.nbf = nbf;
	}
	
	/**
	 * definis la figure selectionnee
	 * @param sel indice de la figure
	 */
	public void setSel(int sel) {
		this.sel = sel;
	}

	/**
	 * Mise � jour de l'affichage Op�rations de s�lection, 
	 * d�selection, changement de position, changement de couleur
	 */
	public void majAffichage() {
		setChanged();
		notifyObservers();
	}

	public void affiche(Graphics g) {
		for(FigureColoree f : lfg) {
			f.affiche(g);
		}
		
	}
	
}
