package modele;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 3.0
 * Classe définissant un quadrilatere
 */
public class Quadrilatere extends Polygone {
	
	/**
	 * Constructeur vide
	 */
	public Quadrilatere() {
		super();
	}
	
	/**
	 * Cette méthode retourne
	 * en résultat le nombre de points de mémorisation d'un quadrilatère.
	 */
	public int nbPoints() {
		return 4;
	}
}
