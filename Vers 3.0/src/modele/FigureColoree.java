package modele;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 3.0
 * Classe abstraite qui est le sommet de la hierarchie des figures
 *
 */
public abstract class FigureColoree {

	private final static int TAILLE_CARRE_SELECTION = 8;//constante de la taille des carr�s de s�l�ction
	private boolean selected;//vrai si figure select, faux sinon
	protected Color couleur; // couleur de la figure
	protected Point[] tab_mem;// tabelau des points de m�morisation de la figure
	private static int PERIPHERIE_CARRE_SELECTION = 8; //Constante d�finissant la taille de la p�riph�rie des carr�s de s�lection. 

	/**
	 * Constructeur vide
	 */
	public FigureColoree() {
		tab_mem = new Point[this.nbPoints()];
		couleur = Color.BLACK;
		selected = false;
	}

	/**
	 * M�thode asbtraite qui retourne le nb de points
	 * de m�morisation
	 * @return nb de points de m�morisation
	 */
	public abstract int nbPoints();

	/**
	 * M�thode asbtraite qui retourne le nb de clics n�cessaires
	 * � la construction d'une figure
	 * @return nb de clics de souris n�cessaires
	 */
	public abstract int nbClics();

	/**
	 * M�thode abstraite qui permet de modifier les pts
	 * de m�morisation � partir de pts de saisie
	 * @param ps tabl de pts qui remplacera le tableau tab_mem
	 */
	public abstract void modifierPoints(Point[] ps);


	/**
	 * m�thode qui affiche la figure s�l�ction�e
	 * � l'aide de petits carr�s
	 * @param g environnement graphique
	 */
	public void affiche (Graphics g) {
		if(selected) {
			
			for(int i =0; i<tab_mem.length; i ++) {
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(tab_mem[i].rendreX() - TAILLE_CARRE_SELECTION / 2, tab_mem[i].rendreY()- TAILLE_CARRE_SELECTION / 2,
						TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);
				
				g.setColor(Color.black);
				g.drawRect(tab_mem[i].rendreX() - TAILLE_CARRE_SELECTION / 2, tab_mem[i].rendreY()- TAILLE_CARRE_SELECTION / 2,
						TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);
			}
			g.setColor(couleur);
		}
	}

	/**
	 * M�thode qui s�lectionne la figure
	 */
	public void selectionne() {
		selected = true;
	}

	/**
	 * m�thode qui d�s�lectionne la figure
	 */
	public void deSelectionne() {
		selected = false;
	}

	/**
	 * m�thode qui change la couleur de la figure
	 * @param c nouvelle couleur
	 */
	public void changeCouleur(Color c) {
		couleur = c;
	}


	/**
	 * M�thode qui d�tecte un point se trouvant pr�s d'un carr� de s�l�ction.
	 * @param x abscisse d'un clic de souris.
	 * @param y ordonn�e d'un clic de souris.
	 * @return l'indice dans tab_mem du point se trouvant pr�s d'un carr� de s�lection.
	 */
	public int carreDeSelection(int x, int y) {
		for(int i = 0; i<tab_mem.length; i++) {
			if(x < tab_mem[i].rendreX() + PERIPHERIE_CARRE_SELECTION && x > tab_mem[i].rendreX()-PERIPHERIE_CARRE_SELECTION &&
					y < tab_mem[i].rendreY() +PERIPHERIE_CARRE_SELECTION && y > tab_mem[i].rendreY()-PERIPHERIE_CARRE_SELECTION) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * M�thode abstratite estDedans
	 * @param x abscisse
	 * @param y ordonn�e
	 * @return vrai si le point dont les coordonn�es 
	 * sont pass�es en param�tre se trouve � l'int�rieur 
	 * de la figure.
	 */
	public abstract boolean estDedans(int x, int y);


	/**
	 * Cette m�thode permet d'effectuer une transformation 
	 * des coordonn�es des points de m�morisation de la figure.
	 * @param dx d�placement sur l'axe des abscisses.
	 * @param dy d�placement sur l'axe des ordonn�es.
	 * @param indice indice dans tab_mem du point � modifier.
	 */
	public void transformation(int dx, int dy, int indice) {
		tab_mem[indice].modifierX(dx);
		tab_mem[indice].modifierY(dy);
	}


	/**
	 * Cette m�thode permet d'effectuer une translation 
	 * des coordonn�es des points de m�morisation de la figure.
	 * @param dx d�placement sur l'axe des abscisses.
	 * @param dy d�placement sur l'axe des ordonn�es.
	 */
	public void translation(int dx, int dy) {

		for(int i=0; i<tab_mem.length; i++) {
			tab_mem[i].translation(dx,dy);
		}
	}





}
