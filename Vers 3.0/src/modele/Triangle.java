package modele;

public class Triangle extends Polygone{

	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le 
	 * nombre de points dont on a besoin, en g�n�ral, pour la saisie d'un polygone.
	 */
	public int nbClics() {
		return 3;
	}

}
