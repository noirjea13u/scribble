package modele;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * classe d�finissant un point
 *
 */
public class Point {

	private int x; // abscisse du point
	private int y;// ordonn�e du point
	
	/**
	 * Constructeur du point
	 * @param x abscisse du point � cr�er
	 * @param y ordonn�e du point � cr�er
	 */
	public Point(int x,int y) {
		this.x = x;
		this.y = y;		
	}
	
	
	/**
	 * M�thode calculant la distance entre deux points
	 * @param p deuxi�me point
	 * @return la distance 
	 */
	public double distance(Point p) {
		return Math.abs(p.rendreX() - this.x) - Math.abs(p.rendreY()-this.y);
	}
	
	
	/**
	 * M�thode renvoyant l'abscisse du point.
	 * @return l'abscisse
	 */
	public int rendreX() {
		return this.x;
	}
	
	/**
	 * M�thode renvoyant l'ordonn�e du point.
	 * @return l'ordonn�e
	 */
	public int rendreY() {
		return this.y;
	}
	
	
	/**
	 * M�thode incr�mentant l'abscisse du point.
	 * @param inc incr�ment � appliquer
	 */
	public void incrementerX(int inc) {
		this.x += inc;
	}
	
	/**
	 * M�thode incr�mentant l'ordonn�e du point.
	 * @param inc incr�ment � appliquer
	 */
	public void incrementerY(int inc) {
		this.y += inc;
	}
	
	
	/**
	 * M�thode modifiant l'abscisse du point.
	 * @param modif nouvelle abscisse
	 */
	public void modifierX(int modif) {
		this.x = modif;
	}
	
	/**
	 * M�thode modifiant l'ordonn�e du point.
	 * @param modif nouvelle ordonn�e
	 */
	public void modifierY(int modif) {
		this.y = modif;
	}
	
	
	/**
	 * M�thode translatant le point
	 * @param incX d�placement en abscisse
	 * @param incY d�placement en ordonn�e
	 */
	public void translation(int incX, int incY) {
		this.x += incX;
		this.y += incY;
	}
	
	
}
