package controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import modele.DessinModele;
import modele.FigureColoree;
import modele.Quadrilatere;
import modele.Rectangle;
import modele.Triangle;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 3.0
 * Classe d�finissant une partie du controleur
 * Classe construisant l'interface utilisateur
 *
 */
public class PanneauChoix extends JPanel{


	private DessinModele dmodele;// modele
	private VueDessin vdessin;//vue
	private FigureColoree fc; //figure en cours de fabrication
	private JRadioButton b3; //Bouton d'acces aux manipulations

	/**
	 * Constructeur de la classe
	 * @param vdessin de type DessinFigure
	 */
	public PanneauChoix(VueDessin vdessin) {
		dmodele = new DessinModele();
		this.vdessin = vdessin;
		dmodele.addObserver(vdessin);

		setLayout(new BorderLayout());

		final JRadioButton b1 = new JRadioButton("Nouvelle figure");
		final JRadioButton b2 = new JRadioButton("Trace a main levee");
		b3 = new JRadioButton("Manipulations");

		ButtonGroup selection = new ButtonGroup();
		selection.add(b1);
		selection.add(b2);
		selection.add(b3);



		b1.setSelected(true);

		JComboBox choixFigure = new JComboBox(new String[] {"quadrilatere","triangle","rectangle"
		});
		choixFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				b1.doClick();				
			}

		});

		JComboBox choixCouleur = new JComboBox(new String[] {
				"noir","rouge","bleu","vert","jaune","al�atoire"
		});



		choixCouleur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(dmodele.getSel() != -1) {
					if(b3.isSelected()) {
						dmodele.changeCoul(dmodele.getLfg().get(dmodele.getSel()),determinerCouleur(choixCouleur.getSelectedIndex()));
					}
					if(!dmodele.getLfg().contains(fc)) {
						fc.changeCouleur(determinerCouleur(choixCouleur.getSelectedIndex()));
					}
				} else {
					fc.changeCouleur(determinerCouleur(choixCouleur.getSelectedIndex()));
				}
				vdessin.repaint();
			}
		});

		dmodele.initDessinModele();

		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				choixFigure.setEnabled(true);
				
				if(dmodele.getSel() != -1) {
					dmodele.getLfg().get(dmodele.getSel()).deSelectionne();
				}
				dmodele.majAffichage();

				vdessin.supprimeAuditeurs();

				fc = creeFigure(choixFigure.getSelectedIndex());

				fc.changeCouleur(determinerCouleur(choixCouleur.getSelectedIndex()));

				vdessin.construit(fc,b3);	

			}	
		});

		b3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				choixFigure.setEnabled(false);
				
				vdessin.supprimeAuditeurs();

				vdessin.activeManipulationSouris();

			}

		});

		b2.setEnabled(false);
		b1.doClick();

		JPanel haut = new JPanel();
		JPanel bas = new JPanel();

		haut.add(b1);
		haut.add(b2);
		haut.add(b3);
		bas.add(choixFigure);
		bas.add(choixCouleur);

		add(haut,BorderLayout.NORTH);
		add(bas,BorderLayout.SOUTH);

	}


	/**
	 * m�thode qui determine la couleur � utiliser
	 * @param index repr�sentant la couleur � utiliser
	 * @return la couleur choisie par l'utilisateur
	 */
	private Color determinerCouleur(int index) {
		switch(index) {
		case 0:
			return Color.black;
		case 1:
			return Color.RED;
		case 2:
			return Color.blue;
		case 3:
			return Color.green;
		case 4:
			return Color.yellow;
		case 5:
			return new Color((int)Math.floor(Math.random()*255), (int)Math.floor(Math.random()*255), (int)Math.floor(Math.random()*255));
		default:
			return Color.BLACK;
		}
	}
	
	/**
	 * M�thode impl�mentant la cr�ation d'une forme g�om�trique.
	 * @param index indice repr�sentant la figure � construire
	 * @return une figure color�e de type choisi par l'utilisateur
	 */
	private FigureColoree creeFigure(int index) {
		switch(index) {
		case(0):
			return new Quadrilatere();
		case(1):
			return new Triangle();
		case(2):
			return new Rectangle();
		default:
			return new Quadrilatere();
		} 

	}	

}
