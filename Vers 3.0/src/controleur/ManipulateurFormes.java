package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import modele.DessinModele;
import modele.FigureColoree;

/**
 * Classe g�rant le d�placement et la 
 * transformation des figures g�om�triques.
 * @author Noirjean Sacha
 * @author Gandiolle ELiott
 * @version 3.0
 *
 */
public class ManipulateurFormes implements MouseListener,MouseMotionListener{

	private DessinModele dm;//MOdele
	private int indice;//Attribut indiquant l'indice du point proche d'un carr� de s�l�ction.
	private int last_x; //Abscisse d'un clic de souris.
	private int last_y; //Ordonn�e d'un clic de souris.
	private ArrayList<FigureColoree> lfg;//Liste de figures du mod�le
	private int nbf;//Nombre effectif de figures apparaissant dans ce dessin.
	private int sel; //Indice de la figure actuellement s�lectionn�e (-1 si aucune figure n'est s�lectionn�e).
	private boolean trans; //Attribut indiquant si un d�placement est en cours.


	/**
	 * Costructeur
	 * @param dm Objet de type DessinMOdele
	 */
	public ManipulateurFormes(DessinModele dm) {
		this.dm = dm;
		this.indice = -1;
		this.lfg = dm.getLfg();
		this.nbf = lfg.size();
		this.sel = dm.getSel();
		this.trans = false;
		this.last_y = -1;
		this.last_x = -1;
	}

	/**
	 * Cette m�thode retourne le nombre de 
	 * figures apparaissant dans ce dessin.
	 * @return nombre de figures sur le dessin.
	 */
	public int nbFigures() {
		return nbf;
	}

	/**
	 * Cette m�thode s�lectionne la prochaine figure dans le 
	 * tableau des figures. (avec un parcours cyclique de ce tableau).
	 */
	public void selectionProchaineFigure() {
		if(lfg.size() > sel+1) {
			lfg.get(sel).deSelectionne();
			lfg.get(sel+1).selectionne();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		if(sel != -1) {

			if(indice != -1) {
				lfg.get(sel).transformation(e.getX(),e.getY(),indice);
				trans = false;

			} else {
				if((indice = lfg.get(sel).carreDeSelection(e.getX(),e.getY())) != -1 && !trans) {
						lfg.get(sel).transformation(e.getX(),e.getY(),indice);
						trans = false;
				}
			} 

			if(trans) {

				trans = true;
				if(last_x != -1) {
					lfg.get(sel).translation(e.getX() - last_x,e.getY() - last_y);
				}

			}

		}

		dm.majAffichage();

		last_x = e.getX();
		last_y = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		boolean nonTrouver = true;		
		
		for(int i = lfg.size() -1; i >= 0; i--) {

			if(lfg.get(i).estDedans(e.getX(),e.getY())){
				if(sel != -1) {
					lfg.get(sel).deSelectionne();
				}
				lfg.get(i).selectionne();
				sel = i;
				dm.setSel(sel);
				trans = true;
				nonTrouver = false;
				break;
			}
		}

		if(sel != -1 && lfg.get(sel).carreDeSelection(e.getX(),e.getY()) != -1) {
			nonTrouver = false;
			trans = false;
		}
		
		if(nonTrouver) {
			if(sel != -1) {
				lfg.get(sel).deSelectionne();
				sel = -1;
				dm.setSel(sel);
			}
		}

		last_x = e.getX();
		last_y = e.getY();

		dm.majAffichage();

	}
	
	/**
	 * Cette m�thode retourne la figure actuellement s�lectionn�e.
	 * @return figure selectionnee
	 */
	public FigureColoree figureSelection() {
		return lfg.get(sel);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		trans = false;		
		indice = -1;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

}
