package vue;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JRadioButton;

import controleur.FabricantFigures;
import controleur.ManipulateurFormes;
import modele.DessinModele;
import modele.FigureColoree;
import modele.Quadrilatere;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 2.0
 * Classe qui d�finit la vue (MVC)
 *
 */
public class VueDessin extends JPanel implements Observer{


	private DessinModele dessin; //dessin qui sera pr�sent sur la vue
	private ManipulateurFormes mf; //Objet "listener" pour les manipulations et transformations de figures via la souris
	/**
	 * Constructeur de la vue
	 */
	public VueDessin() {
		super();
	}
	
	/**
	 * Getter de dessin
	 * @return le dessin modele
	 */
	public DessinModele getDessin() {
		return dessin;
	}


	@Override
	public void update(Observable o,Object arg1) {
		dessin = (DessinModele) o;
		repaint();
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(dessin != null) {
			
			dessin.affiche(g);
			
		}
	}
	
	/**
	 * Cette m�thode permet d'initier le m�canisme �v�nementiel de 
	 * fabrication des figures � la souris (ajout du listener).
	 * @param fc forme � construire point par point � la souris
	 * @param manipulation bouton permettant de passer en mode manipulation
	 */
	public void construit(FigureColoree fc,JRadioButton manipulation) {
		this.addMouseListener(new FabricantFigures(fc,manipulation));
	}
	
	/**
	 * M�thode activant les manipulations des formes g�om�triques � la souris.
	 */
	public void activeManipulationSouris() {
		if(dessin.getLfg().size() > 0) {
			mf = new ManipulateurFormes(dessin);
			this.addMouseListener(mf);
			this.addMouseMotionListener(mf);

		}
	}
	
	/**
	 * M�thode d�sactivant les manipulations des formes g�om�triques � la souris.
	 */
	public void desactiveManipulationsSouris() {
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
	}
	
	/**
	 * Permet de supprimer tous les actuels auditeurs
	 */
	public void supprimeAuditeurs() {
		MouseListener[] ml = this.getMouseListeners();
		for(MouseListener m : ml) {
			this.removeMouseListener(m);
		}
		if(mf != null) {
			this.removeMouseMotionListener(mf);
		}
	}
	
	/**
	 * Cette m�thode retourne la figure actuellement s�lectionn�e.
	 * @return figure s�lectionn�e (ou null) si aucune figure n'est s�lectionn�e.
	 */
	public FigureColoree figureSelection() {
		if(mf != null) {
			return mf.figureSelection();
		}
		return null;
	}
	
	
	

}
