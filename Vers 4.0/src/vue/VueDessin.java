package vue;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JRadioButton;

import controleur.FabricantFigures;
import controleur.ManipulateurFormes;
import controleur.TraceurForme;
import modele.DessinModele;
import modele.Dessinable;
import modele.FigureColoree;
import modele.Quadrilatere;
import modele.Trait;

/**
 * Classe qui d�finit la vue (MVC)
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class VueDessin extends JPanel implements Observer{


	private DessinModele dessin; //dessin qui sera pr�sent sur la vue
	private ManipulateurFormes mf; //Objet "listener" pour les manipulations et transformations de figures via la souris//Tableau des traits
	private TraceurForme tf; //Trac� � main lev�e
	private boolean nuit; //booleen indiquant si le fond de la fenetre est en mode nuit ou non
	
	/**
	 * Constructeur de la vue
	 */
	public VueDessin() {
		super();
		nuit = true;
	}
	
	/**
	 * Getter de dessin
	 * @return le dessin modele
	 */
	public DessinModele getDessin() {
		return dessin;
	}
	
	/**
	 * Permet de changer la valeur du booleen nuit
	 */
	public void changerModeJourNuit() {
		nuit = !nuit;
	}


	@Override
	public void update(Observable o,Object arg1) {
		dessin = (DessinModele) o;
		repaint();
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(nuit) {
			g.setColor(Color.black);
		} else {
			g.setColor(Color.white);
		}
		g.fillRect(0,0,getWidth(),getHeight());
		for(Dessinable d : dessin.getLfg()) {
			d.affiche(g);
		}
		
		
	}
	
	/**
	 * Cette m�thode permet d'initier le m�canisme �v�nementiel de 
	 * fabrication des figures � la souris (ajout du listener).
	 * @param fc forme � construire point par point � la souris
	 * @param manipulation bouton permettant de passer en mode manipulation
	 */
	public void construit(FigureColoree fc,JRadioButton manipulation) {
		this.addMouseListener(new FabricantFigures(fc,manipulation));
	}
	
	/**
	 * M�thode activant les manipulations des formes g�om�triques � la souris.
	 */
	public void activeManipulationSouris() {
		if(dessin.getLfg().size() > 0) {
			mf = new ManipulateurFormes(dessin);
			this.addMouseListener(mf);
			this.addMouseMotionListener(mf);
		}
	}
	
	/**
	 * M�thode d�sactivant les manipulations des formes g�om�triques � la souris.
	 */
	public void desactiveManipulationsSouris() {
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
	}
	
	/**
	 * Permet de supprimer tous les actuels auditeurs
	 */
	public void supprimeAuditeurs() {
		MouseListener[] ml = this.getMouseListeners();
		for(MouseListener m : ml) {
			this.removeMouseListener(m);
		}
		
		MouseMotionListener[] ml1 = this.getMouseMotionListeners();
		for(MouseMotionListener m1 : ml1) {
			this.removeMouseMotionListener(m1);
		}
	}
	
	/**
	 * Cette m�thode retourne la figure actuellement s�lectionn�e.
	 * @return figure s�lectionn�e (ou null) si aucune figure n'est s�lectionn�e.
	 */
	public FigureColoree figureSelection() {
		if(mf != null) {
			return mf.figureSelection();
		}
		return null;
	}
	
	
	/**
	 * Cette m�thode permet d'initier le m�canisme �v�nementiel 
	 * de trac� quelconque � la souris (d�finition de la couleur 
	 * du trac� et ajout des listeners).
	 * @param c couleur du trait
	 * @param epaisseur epaisseur du trait
	 */
	public void trace(Color c,int epaisseur) {
		tf = new TraceurForme(dessin);
		tf.setColor(c);
		tf.setEpaisseur(epaisseur);
		this.addMouseListener(tf);
		this.addMouseMotionListener(tf);
	}
	

}
