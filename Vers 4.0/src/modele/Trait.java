package modele;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;

/**
 * Classe Trait pour le trac� � main lev�e
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class Trait implements Dessinable {
	
	private int debx;
	private int deby;
	private int finx;
	private int finy;
	private Color couleur;
	private int epaisseur;
	
	/**
	 * constructeur d'un trait
	 * @param x abscisse de d�part du trait
	 * @param y ordonn�e de d�part du trait
	 * @param x2 abscisse d'arriv�e du trait
	 * @param y2 ordonn�e d'arriv�e du trait
	 * @param c couleur du trait
	 * @param e epaisseur
	 */
	public Trait(int x,int y,int x2,int y2,Color c,int e) {
		debx = x;
		deby = y;
		finx = x2;
		finy = y2;
		couleur = c;	
		epaisseur = e;
	}
	
	public void affiche(Graphics g) {
		Graphics2D g1 = (Graphics2D) g;
		g1.setStroke(new BasicStroke(epaisseur));
		g1.setColor(couleur);
		g1.drawLine(debx,deby,finx,finy);
		g1.setStroke(new BasicStroke(1));
	}

	public int getDebx() {
		return debx;
	}

	public void setDebx(int debx) {
		this.debx = debx;
	}

	public int getDeby() {
		return deby;
	}

	public void setDeby(int deby) {
		this.deby = deby;
	}

	public int getFinx() {
		return finx;
	}

	public void setFinx(int finx) {
		this.finx = finx;
	}

	public int getFiny() {
		return finy;
	}

	public void setFiny(int finy) {
		this.finy = finy;
	}

	public Color getCouleur() {
		return couleur;
	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	
	public int getEpaisseur() {
		return epaisseur;
	}
	
	
}
