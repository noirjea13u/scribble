package modele;

import java.awt.Graphics;

public interface Dessinable {
	
	public void affiche(Graphics g);
	
}
