package modele;

/**
 * Classe définissant un quadrilatere
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class Quadrilatere extends Polygone {
	
	/**
	 * Constructeur vide
	 */
	public Quadrilatere() {
		super();
	}
	
	/**
	 * Cette méthode retourne
	 * en résultat le nombre de points de mémorisation d'un quadrilatère.
	 */
	public int nbPoints() {
		return 4;
	}
}
