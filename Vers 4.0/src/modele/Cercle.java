package modele;

import java.awt.Graphics;

/**
 * Classe de construction d'un cercle
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 *
 */
public class Cercle extends ConiqueCentree{

	private double rayon;//Rayon du cercle.
	
	
	/**
	 * Ce constructeur instancie un cercle vide.
	 */
	public Cercle() {
		super();
	}
	
	@Override
	public int nbPoints() {
		return 2;
	}

	@Override
	public int nbClics() {
		return 2;
	}

	@Override
	public void modifierPoints(Point[] ps) {
		tab_mem = ps;
		centre = ps[0];
		rayon = centre.distance(ps[1]);
	}
	
	public void affiche(Graphics g) {
		
		int r = (int) rayon;
		g.setColor(couleur);
		g.fillOval(centre.rendreX() - r,
				centre.rendreY() - r,
				 r*2,r*2);
		super.affiche(g);
		
	}

	@Override
	public boolean estDedans(int x, int y) {
		return centre.distance(new Point(x,y)) <= rayon;
	
	}
	
	public double rendreRayon() {
		return rayon;
	}
	
	public void transformation(int dx,int dy, int indice) {
		
		tab_mem[indice].modifierX(dx);
		tab_mem[indice].modifierY(dy);
		if(indice == 1) {
			rayon = centre.distance(tab_mem[1]);
		} else {
			centre = tab_mem[0];
			tab_mem[1] = new Point(centre.rendreX() + (int)rayon,centre.rendreY());
		}
		
	}
	
	public void translation(int dx,int dy) {
		centre.incrementerX(dx);
		centre.incrementerY(dy);
		tab_mem[1].incrementerX(dx);
		tab_mem[1].incrementerY(dy);
		
		tab_mem[0] = centre;

	}
	
	

}
