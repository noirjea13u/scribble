package modele;

import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Classe d�finissant un polygone
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public abstract class Polygone extends FigureColoree {
	
	private Polygon p; // polygone
	
	/**
	 * Constructeur vide
	 */
	public Polygone() {
		super();
	}
	
	/**
	 * Cette m�thode modifie le polygone
	 * conform�ment � un ensemble de points de saisie (ses nouveaux sommets).
	 */
	public void affiche(Graphics g) {
		p = new Polygon();
		for(int i = 0; i < tab_mem.length;i++) {
			p.addPoint(
					tab_mem[i].rendreX(),
					tab_mem[i].rendreY());
		}
		g.setColor(couleur);
		g.fillPolygon(p);
		super.affiche(g);
	}
	
	
	/**
	 * Cette m�thode retourne en r�sultat le 
	 * nombre de points dont on a besoin, en g�n�ral, pour la saisie d'un polygone.
	 */
	public int nbClics() {
		return 4;
	}
	
	/**
	 * Cette m�thode modifie le polygone conform�ment 
	 * � un ensemble de points de saisie (ses nouveaux sommets).
	 */
	public void modifierPoints(Point[] tab_saisie) {
		tab_mem = tab_saisie;
	}
	
	/**
	 * M�thode abstratite estDedans
	 * @param x abscisse
	 * @param y ordonn�e
	 * @return vrai si le point dont les coordonn�es 
	 * sont pass�es en param�tre se trouve � l'int�rieur 
	 * de la figure.
	 */
	public  boolean estDedans(int x, int y) {
		return p.contains(x, y);
	}
}
