package modele;

/**
 *  classe losange
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class Losange extends Quadrilatere {

	/**
	 * Constructeur du losange
	 */
	public Losange() {
		super();
	}

	/**
	 * Cette m�thode retourne en r�sultat le nombre de 
	 * points dont on a besoin pour la saisie d'un losange.
	 */
	public int nbClics() {
		return 2;
	}

	/**
	 * Cette m�thode modifie le losange conform�ment � un ensemble de deux points de 
	 * saisie. Ces deux points forment un cote du losange et le reste se complete
	 * @param tableau contenant les nouveaux points de saisie
	 */
	public void modifierPoints(Point[] tab_saisie) {
		tab_mem[0] = tab_saisie[0];
		tab_mem[1] = tab_saisie[1];
		tab_mem[2] = new Point(tab_saisie[0].rendreX(),tab_saisie[1].rendreY() + (tab_saisie[1].rendreY() - tab_saisie[0].rendreY()));
		tab_mem[3] = new Point(tab_saisie[0].rendreX() + (tab_saisie[0].rendreX() - tab_saisie[1].rendreX()),tab_saisie[1].rendreY());


	}

	/**
	 * Cette m�thode permet d'effectuer une transformation 
	 * des coordonn�es des points de m�morisation du losange.
	 * @param dx d�placement sur l'axe des abscisses.
	 * @param dy d�placement sur l'axe des ordonn�es.
	 * @param indice indice dans tab_mem du point � modifier.
	 */
	public void transformation(int dx, int dy, int indice) {

		int indicePaire = 0;
		int deplacementOppose;
		switch(indice) {
		case(0):
			indicePaire = 2;
			break;
		case(1):
			indicePaire = 3;
			break;
		case(2):
			indicePaire = 0;
			break;
		case(3):
			indicePaire = 1;
			break;
		}
		
		if(indice % 2 == 0) {
			deplacementOppose = tab_mem[indice].rendreY() - dy;
			tab_mem[indice].modifierY(dy);
			tab_mem[indicePaire].incrementerY(deplacementOppose);
		} else {
			deplacementOppose = tab_mem[indice].rendreX() - dx;
			tab_mem[indice].modifierX(dx);
			tab_mem[indicePaire].incrementerX(deplacementOppose);
		}
		



	}

}

