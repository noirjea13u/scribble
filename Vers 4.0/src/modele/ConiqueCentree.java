package modele;

/**
 * Cette classe abstraite est 
 * la super classe de Cercle et de toute conique centr�e en g�n�ral.
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public abstract class ConiqueCentree extends FigureColoree{

	protected Point centre; //Centre de la conique centr�e.
	
	/**
	 * Constructeur vide
	 */
	public ConiqueCentree() {
		super();
	}
	
	/**
	 * Retourne le centre de la conique (premier
	 *  point de m�morisation)
	 * @return le centre
	 */
	public Point rendreCentre() {
		return centre;
	}
	
}
