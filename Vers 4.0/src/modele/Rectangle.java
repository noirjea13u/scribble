package modele;

/**
 * Classe rectangle
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class Rectangle extends Quadrilatere {

	/**
	 * Constructeur du rectangle
	 */
	public Rectangle() {
		super();
	}

	/**
	 * Cette m�thode retourne en r�sultat le nombre de 
	 * points dont on a besoin pour la saisie d'un rectangle.
	 */
	public int nbClics() {
		return 2;
	}

	/**
	 * Cette m�thode modifie le rectangle conform�ment � un ensemble de deux points de 
	 * saisie. Ces deux points forment une diagonale du rectangle 
	 * (du coin sup�rieur gauche vers le coin inf�rieur droit).
	 * @param tab_saisie contenant les nouveaux points de saisie
	 */
	public void modifierPoints(Point[] tab_saisie) {
		tab_mem[0] = tab_saisie[0];
		tab_mem[1] = new Point(tab_saisie[0].rendreX(),tab_saisie[1].rendreY());
		tab_mem[2] = tab_saisie[1];
		tab_mem[3] = new Point(tab_saisie[1].rendreX(),tab_saisie[0].rendreY());


	}

	/**
	 * Cette m�thode permet d'effectuer une transformation 
	 * des coordonn�es des points de m�morisation du rectangle.
	 * @param dx d�placement sur l'axe des abscisses.
	 * @param dy d�placement sur l'axe des ordonn�es.
	 * @param indice indice dans tab_mem du point � modifier.
	 */
	public void transformation(int dx, int dy, int indice) {

		int indicePaire = 0;
		int indicePaire2 = 0;
		switch(indice) {
		case(0):
			indicePaire = 1;
			indicePaire2 = 3;
			break;
		case(1):
			indicePaire = 0;
			indicePaire2 = 2;
			break;
		case(2):
			indicePaire = 3;
			indicePaire2 = 1;
			break;
		case(3):
			indicePaire = 2;
			indicePaire2 = 0;
			break;
		}
		tab_mem[indice].modifierX(dx);
		tab_mem[indicePaire].modifierX(dx);

		tab_mem[indice].modifierY(dy);
		tab_mem[indicePaire2].modifierY(dy);


	}

}
