package modele;

/**
 * Classe triangle
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class Triangle extends Polygone{
	
	/**
	 * Ce constructeur instancie un triangle avec
	 *  des sommets al�atoires (via le constructeur de Polygone).
	 */
	public Triangle() {
		super();
	}

	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le 
	 * nombre de points dont on a besoin, en g�n�ral, pour la saisie d'un polygone.
	 */
	public int nbClics() {
		return 3;
	}

}
