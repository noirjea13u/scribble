package controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import modele.*;
import vue.VueDessin;

/**
 *  Classe d�finissant une partie du controleur
 * Classe construisant l'interface utilisateur
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class PanneauChoix extends JPanel{


	private DessinModele dmodele;// modele
	private TraceurForme trait;// modele
	private VueDessin vdessin;//vue
	private FigureColoree fc; //figure en cours de fabrication
	private JRadioButton manipulations; //Bouton d'acces aux manipulations
	private boolean nuit; // Est on en mode nuit ou jour
	private Color couleurActuelle; // Couleur de fond

	/**
	 * Constructeur de la classe
	 * @param vdessin de type DessinFigure
	 */
	public PanneauChoix(VueDessin vdessin) {
		nuit = false;
		dmodele = new DessinModele();
		this.vdessin = vdessin;
		dmodele.addObserver(vdessin);
		couleurActuelle = Color.black;

		setLayout(new BorderLayout());

		final JRadioButton figures = new JRadioButton("Nouvelle figure");
		final JRadioButton scribble = new JRadioButton("Trace a main levee");
		manipulations = new JRadioButton("Manipulations");

		ButtonGroup selection = new ButtonGroup();
		selection.add(figures);
		selection.add(scribble);
		selection.add(manipulations);

		
		JSlider epaisseurT = new JSlider(1,10,2);
		epaisseurT.setPreferredSize(new Dimension(200,30));
		epaisseurT.setMajorTickSpacing(1);
		epaisseurT.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				scribble.doClick();				
			}
			
		});

		figures.setSelected(true);

		JComboBox choixFigure = new JComboBox(new String[] {"quadrilatere","triangle","rectangle","cercle","losange"
		});
		choixFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				figures.doClick();				
			}

		});

		JButton choixCouleur = new JButton(new ImageIcon("ColorJour.png"));
		choixCouleur.setPreferredSize(new Dimension(30,30));
		
		
		choixCouleur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Color nouvelleCouleur = JColorChooser.showDialog(null,"Choisir une couleur",Color.black); 
					
				
				if(dmodele.getSel() != -1) {
					if(manipulations.isSelected()) {
						dmodele.changeCoul((FigureColoree)dmodele.getLfg().get(dmodele.getSel()),couleurActuelle);
					}

					if(!dmodele.getLfg().contains(fc)) {
						fc.changeCouleur(couleurActuelle);
					}
				} else if(scribble.isSelected()){
					scribble.doClick();
				} else {
					if(figures.isSelected()) {
						fc.changeCouleur(couleurActuelle);
					}
				}
				vdessin.repaint();
			}

		});

		dmodele.initDessinModele();

		figures.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				epaisseurT.setVisible(false);
				choixFigure.setVisible(true);

				if(dmodele.getSel() != -1) {
					((FigureColoree)dmodele.getLfg().get(dmodele.getSel())).deSelectionne();
					dmodele.setSel(-1);
				}
				dmodele.majAffichage();

				vdessin.supprimeAuditeurs();

				fc = creeFigure(choixFigure.getSelectedIndex());

				fc.changeCouleur(couleurActuelle);

				vdessin.construit(fc,manipulations);	

			}	
		});

		scribble.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(dmodele.getSel() != -1) {
					((FigureColoree)dmodele.getLfg().get(dmodele.getSel())).deSelectionne();
					dmodele.setSel(-1);
				}
				dmodele.majAffichage();
				
				epaisseurT.setVisible(true);
				choixFigure.setVisible(false);

				vdessin.supprimeAuditeurs();

				vdessin.trace(couleurActuelle,epaisseurT.getValue());

			}	
		});

		manipulations.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				epaisseurT.setVisible(false);
				choixFigure.setVisible(false);

				vdessin.supprimeAuditeurs();

				vdessin.activeManipulationSouris();

			}

		});
	

		figures.doClick();

		JPanel haut = new JPanel();
		JPanel bas = new JPanel();

		

		JButton modeJourNuit = new JButton(new ImageIcon("BoutonJour.png"));
		modeJourNuit.setPreferredSize(new Dimension(30,30));
		modeJourNuit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				Color mode;
				Color police;
				if(nuit) {
					mode = new Color(36,66,124);
					modeJourNuit.setIcon(new ImageIcon("BoutonNuit.png"));
					choixCouleur.setIcon(new ImageIcon("ColorNuit.png"));
					nuit = false;
					vdessin.changerModeJourNuit();
					police = Color.white;
				} else {
					mode = new Color(119,181,254);
					choixCouleur.setIcon(new ImageIcon("ColorJour.png"));
					modeJourNuit.setIcon(new ImageIcon("BoutonJour.png"));
					nuit = true;
					vdessin.changerModeJourNuit();
					police = Color.black;
				}
				
				dmodele.majAffichage();
				figures.setBackground(mode);
				figures.setForeground(police);
				scribble.setBackground(mode);
				scribble.setForeground(police);
				manipulations.setBackground(mode);
				manipulations.setForeground(police);		
				epaisseurT.setBackground(mode);
				epaisseurT.setForeground(police);
				haut.setBackground(mode);
				bas.setBackground(mode);


			}

		});

		haut.add(figures);
		haut.add(scribble);
		haut.add(manipulations);
		bas.add(modeJourNuit);
		bas.add(choixCouleur);
		bas.add(epaisseurT);
		bas.add(choixFigure);
		modeJourNuit.doClick();
		
		add(haut,BorderLayout.NORTH);
		add(bas,BorderLayout.SOUTH);

	}

	/**
	 * M�thode impl�mentant la cr�ation d'une forme g�om�trique.
	 * @param index indice repr�sentant la figure � construire
	 * @return une figure color�e de type choisi par l'utilisateur
	 */
	private FigureColoree creeFigure(int index) {
		switch(index) {
		case(0):
			return new Quadrilatere();
		case(1):
			return new Triangle();
		case(2):
			return new Rectangle();
		case(3):
			return new Cercle();
		case(4):
			return new Losange();
		default:
			return new Quadrilatere();
		} 

	}	

}
