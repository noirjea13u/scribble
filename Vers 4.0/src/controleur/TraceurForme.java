package controleur;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import modele.DessinModele;
import modele.Dessinable;
import modele.Trait;
import vue.VueDessin;

/**
 * Classe TraceurForme pour le trac� � main lev�e
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class TraceurForme implements MouseListener,MouseMotionListener {

	private int last_x; //Abscisse d'un clic de souris.
	private int last_y; //Ordonnee d'un clic de souris.
	private Color couleur_trait; //Couleur du trait � dessiner.
	private int epaisseur_trait; // epaisseur du trait
	private DessinModele dm; //Dessin

	/**
	 * Constructeur de la classe
	 * @param d Dessin modele
	 */
	public TraceurForme(DessinModele d) {
		last_x = -1;
		last_y = -1;
		dm = d;
	}

	/**
	 * M�thode pour changer la couleur du trait.
	 * @param c nouvelle couleur
	 */
	public void setColor(Color c) {
		couleur_trait = c;
	}
	
	/**
	 * setteur de l'�paisseur
	 * @param epaisseur du trait
	 */
	public void setEpaisseur(int epaisseur) {
		epaisseur_trait = epaisseur;
	}


	@Override
	/**
	 * M�thode effectuant le trace � main levee
	 * @param e mouse event
	 */
	public void mouseDragged(MouseEvent e) {	
		dm.ajoute(new Trait(last_x,last_y,
				e.getX(),e.getY(),couleur_trait,epaisseur_trait));
		
		last_x = e.getX();
		last_y = e.getY();
		
		dm.majAffichage();
		
	}	

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mousePressed(MouseEvent e) {
		
		last_x = e.getX();
		last_y = e.getY();
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	

}
