package controleur;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JRadioButton;

import modele.*;
import vue.VueDessin;

/**
 * Classe impl�mentant la cr�ation de 
 * figures g�om�triques via des clics de souris
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 4.0
 */
public class FabricantFigures implements MouseListener {

	private int nb_points_cliques;//Accumule le nb de clics de souris
	private Point[] points_cliques;// tableau contenant des points cr�es � partir de clics de souris
	private FigureColoree figure_en_cours_de_fabrication; // figures en cours de fabrication
	private JRadioButton manipulation; //Permet de passer en mode manipulation apres la creation d une figure

	/**
	 * Constructeur de la classe
	 * @param f figure vide cr�e dans la classe dialogue
	 * @param manipulation bouton "manipulation" de PanneauChoix
	 */
	public FabricantFigures(FigureColoree f,JRadioButton manipulation) {
		figure_en_cours_de_fabrication = f;
		nb_points_cliques = 0;
		points_cliques = new Point[f.nbClics()];
		this.manipulation = manipulation;
	}


	/**
	 * M�thodes de l'interface MouseListener non utilis�es.
	 * @param e mouse event
	 */
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}
	public void mouseClicked(MouseEvent e) {	
	}

	/**
	 * M�thode impl�mentant la cr�ation d'une figure g�om�trique via des clics de souris.
	 * @param e mouse event
	 */
	public void mousePressed(MouseEvent e) {
		if(figure_en_cours_de_fabrication != null) {
			if(nb_points_cliques<points_cliques.length-1) {
				points_cliques[nb_points_cliques] = new Point(e.getX(), e.getY());
				nb_points_cliques = nb_points_cliques +1;
				
			}else {
				points_cliques[nb_points_cliques] = new Point(e.getX(), e.getY());
				figure_en_cours_de_fabrication.modifierPoints(points_cliques);
				DessinModele dm = (((VueDessin)(e.getSource())).getDessin());
				dm.ajoute(figure_en_cours_de_fabrication);
				
				manipulation.doClick();
				((VueDessin)(e.getSource())).removeMouseListener(this);
			}
		}


	}
}
