package modele;

import java.util.Observable;

import controleur.PanneauChoix;
import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 1.0
 * Classe qui d�finit le mod�le de dessin
 * Toutes les instances de cette classe seront des dessins qui
 * comporteront des figures g�ometriques visualisables � l'�cran
 *
 */
public class DessinModele extends Observable {

	/*
	 * Constructeur du dessin mod�le
	 */
	public DessinModele() {
		super();
	}
	
}
