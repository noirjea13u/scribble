package controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import modele.DessinModele;

import vue.VueDessin;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 1.0
 * Classe d�finissant une partie du controleur
 * Classe construisant l'interface utilisateur
 *
 */
public class PanneauChoix extends JPanel{

	
	private DessinModele dmodele;// mod�le
	private VueDessin vdessin;//vue
	
	/**
	 * Constructeur de la classe
	 * @param vdessin de type DessinFigure
	 */
	public PanneauChoix(VueDessin vdessin) {
		this.vdessin = vdessin;
		dmodele = new DessinModele();
		dmodele.addObserver(this.vdessin);
		
		setLayout(new BorderLayout());
		
		final JRadioButton b1 = new JRadioButton("Nouvelle figure");
		final JRadioButton b2 = new JRadioButton("Trace a main levee");
		final JRadioButton b3 = new JRadioButton("Manipulations");
		
		ButtonGroup selection = new ButtonGroup();
		selection.add(b1);
		selection.add(b2);
		selection.add(b3);
		
		b1.setSelected(true);
		
		JComboBox choixFigure = new JComboBox(new String[] {
				"quadrilatere"
		});
		choixFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		JComboBox choixCouleur = new JComboBox(new String[] {
				"noir","rouge","bleu","vert"
		});
		
		choixCouleur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		choixFigure.setEnabled(false);
		b2.setEnabled(false);
		b3.setEnabled(false);
		
		JPanel haut = new JPanel();
		JPanel bas = new JPanel();
		
		haut.add(b1);
		haut.add(b2);
		haut.add(b3);
		bas.add(choixFigure);
		bas.add(choixCouleur);
		
		add(haut,BorderLayout.NORTH);
		add(bas,BorderLayout.SOUTH);

	}
	
	
	/**
	 * m�thode qui determine la couleur � utiliser
	 * @param index repr�sentant la couleur � utiliser
	 * @return la couleur choisie par l'utilisateur
	 */
	private Color determinerCouleur(int index) {
		switch(index) {
		case 0:
			return Color.RED;
		case 1:
			return Color.GREEN;
		case 2:
			return Color.blue;
		case 3:
			return Color.yellow;
		default :
			return Color.black;
		}
	}
	
}
