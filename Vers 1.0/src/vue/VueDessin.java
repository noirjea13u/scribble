package vue;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import modele.DessinModele;

/**
 * @author Noirjean Sacha
 * @author Gandiolle Eliott
 * @version 1.0
 * Classe qui d�finit la vue (MVC)
 *
 */
public class VueDessin extends JPanel implements Observer{


	private DessinModele modele; //dessin qui sera pr�sent sur la vue

	/*
	 * Constructeur de la vue
	 */
	public VueDessin() {
		super();
	}


	@Override
	public void update(Observable o,Object arg1) {
		modele = (DessinModele) o;
		repaint();
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}



}
